# -*- coding: utf-8 -*-
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from time import sleep
import src.controller.relay as relay
from src.lib.bluetooth import BluetoothController


class RelayTest(unittest.TestCase):
    """Advanced test cases."""

    def test_write_relay(self):
        x = 0
        while x < 1:
            print("entering write relay test method")
            rl = relay.RelayController()
            relaymac1 = "4C:3F:D3:02:B8:F3"
            #relaymac2 = "18:93:D7:0A:82:C7"

            sleep(60)
            print("relay 1 on")
            rl.write_sensor_data(relaymac1, 1, "on")
            sleep(30)
            print("relay 1 off")
            rl.write_sensor_data(relaymac1, 1, "off")
            sleep(1)

            print("relay 2 on")
            rl.write_sensor_data(relaymac1, 2, "on")
            sleep(30)
            print("relay 2 off")
            rl.write_sensor_data(relaymac1, 2, "off")

            x += 1
            print("iteration # " + str(x))

    def test_close_relays(self):
        print("entering test to close all open relays")
        #rl = relay.RelayController()
        #rl.close_all_relays()


if __name__ == "__main__":
    unittest.main()
