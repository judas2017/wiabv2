# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from retrying import retry


class RetryingTest(unittest.TestCase):
    """Advanced test cases."""

    @retry(stop_max_attempt_number=5, wait_fixed=2000)
    def test_retry_false_result(self):
        x = 0
        print("iteration %s" % str(x))
        x = x + 1
        try:
            raise Exception("bad thing happened")
        except Exception as e:
            print("Exception %s caught" % e)
            raise
        finally:
            print("done")


if __name__ == "__main__":
    unittest.main()
