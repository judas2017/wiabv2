# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time

import src.sensor.atlas_i2c as ai

class I2cAtlasTest(unittest.TestCase):
    
    """Advanced test cases."""
    def test_getTemp(self):
        probe = ai.Atlas_I2C()
        # test temp sensor !
        probe.set_i2c_address(102)
        temp = probe.query("R")
        print (temp ,"C")
        self.assertTrue(int(temp) is not 0 )

    def test_getEC(self):
        # ec sensor
        probe = ai.Atlas_I2C()
        probe.set_i2c_address(100)
        temp = probe.query("R")
        print (temp , "ec")
        # sensor should return zero if in air or clean water
        self.assertTrue(int(temp) is not 0 )

    def test_getPH(self):
        probe = ai.Atlas_I2C()
        # ph sensor 
        probe.set_i2c_address(99)
        temp = probe.query("R")
        print (temp , "ph")
        self.assertTrue(int(temp) is not 0 )

    def test_updateFirebase(self):
        probe = ai.Atlas_I2C()
        probe.update_firebase_atlasi2c()
        print("firebase collection updated")


if __name__ == "__main__":
    unittest.main()
