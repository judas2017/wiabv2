# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from bluepy.btle import *

#import src.sensor.thunderboard as thunderboard


class ThunderboardTest(unittest.TestCase):
    """Advanced test cases."""
    errcnt = 0

    def test_retryConnections(self):
        x = 1
        while True:
            self.connect("90:FD:9F:7B:85:35")
            self.connect("00:0B:57:1A:B7:DE")
            x = x +1
            print ("ITERATION : %s" % x)
           
    def connect(self,addr):
        ble_service = Peripheral()
        timestart = time.time()

        try:
            print ("connecting to %s" % addr)
            ble_service = Peripheral(addr)
            print("char len: %s" % len(ble_service.getCharacteristics()))
        except Exception as e:
            self.errcnt = self.errcnt + 1
            print("exception: %s, error count: %s") % (e, self.errcnt)
        finally:
            try:
                print("time duration: %s" % str(time.time() - timestart))
                ble_service.disconnect()
            except Exception as es:
                print("exception while disconnecting %s" % es)

if __name__ == "__main__":
    unittest.main()
