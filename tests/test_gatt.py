# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
import pygatt

#import src.lib.devicelist


class GATTTests(unittest.TestCase):
    """Advanced test cases."""
    device = object
    adapter = pygatt.GATTToolBackend()

    def test_getThunderboards(self):
        self.char = dict()

        self.adapter.start()
        #devices = adapter.scan(1)  #90:FD:9F:7B:85:35
        #print(devices)
        #        for dev in devices:
        #print(dev)
        #           if dev['name'] is not None and "Thunder" in dev['name']:
        print("connecting to %s" % "90:FD:9F:7B:85:35")
        self.device = self.adapter.connect("90:FD:9F:7B:85:35")

        characteristics = self.device.discover_characteristics()
        print("characteristics: %s" % characteristics)

        from src.sensor.thunderboard_data import ThunderboardDO
        tb = ThunderboardDO(characteristics, self.device)
        print("Temp: %s" % tb.read_temperature())
        print("Humidity: %s" % tb.read_humidity())
        print("Light: %s" % tb.read_ambient_light())
        print("UV: %s" % tb.read_uv_index())
        print("CO2: %s" % tb.read_co2())
        print("VOC: %s" % tb.read_voc())
        print("Sound: %s" % tb.read_sound())
        print("Pressure: %s" % tb.read_pressure())


if __name__ == "__main__":
    unittest.main()
