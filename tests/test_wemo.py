# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
import src.controller.wemo as wemo


class WemoControllerTest(unittest.TestCase):
    """Advanced test cases."""

    # note - tests must be run in serial otherwise bind error
    def wemo_on(self):
        wm = wemo.WemoController()
        self.assertTrue(wm.set_wemo_state("Tent Light 2", True))

    def wemo_off(self):
        wm = wemo.WemoController()
        self.assertTrue(wm.set_wemo_state("Tent Light 2", False))

    def test_wemo_leak(self):
        x = 0

        while x < 3:
            self.wemo_on()
            self.wemo_off()
            x += 1


#    def test_wemo_fail(self):
#test that method returns false when wemo switch is not found
#       wm = wemo.WemoController()
#      self.assertFalse(wm.set_wemo_state("NOT_A_SWITCH_NAME", True))

if __name__ == "__main__":
    unittest.main()
