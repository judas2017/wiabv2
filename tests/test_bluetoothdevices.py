# -*- coding: utf-8 -*-
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from src.lib.bluetooth import BluetoothController as btcontroller


class BluetoothControllerTest(unittest.TestCase):
    """Advanced test cases."""

    def test_scan_devices(self):
        x = 0
        while x < 2:
            print("entering test_scan_devices")
            self.assertTrue(btcontroller().scan_for_devices(20))
            x += 1


if __name__ == "__main__":
    unittest.main()
