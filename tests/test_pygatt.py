# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
import logging
import pygatt


class PygattTest(unittest.TestCase):
    """Advanced test cases."""

    def test_write2relay(self):
        addr = "4c:3f:d3:02:b8:f3"
        #"1on": "0xA00101A2",
        #"1off": "0xA00100A1",
        logging.basicConfig()
        logging.getLogger('pygatt').setLevel(logging.DEBUG)
        adapter = pygatt.BGAPIBackend()

        logging.debug("Connecting to dsd relay %s", addr)
        logging.debug("getting the adapter")

        adapter = pygatt.GATTToolBackend()
        adapter.start()
        logging.debug("connecting the adapter addrtype BLEAddressType Public")
        device = adapter.connect(
            addr, address_type=pygatt.BLEAddressType.public)
        logging.debug("connection established")
        #asc = binascii.a2b_hex(code)

        code = "A00101A2"  #1 on;  off: "0xA00100A1",
        #b = bytearray(code, 'utf-8')
        b = bytearray.fromhex(code)
        #b = bytearray(["A0 01 01 A2", "utf-8"])

        logging.debug("writting to the characteristic")
        #handle: 0x0024, char properties: 0x16, char value handle: 0x0025, uuid: 0000ffe1-0000-1000-8000-00805f9b34fb
        device.char_write("0000ffe1-0000-1000-8000-00805f9b34fb", b, True)

        logging.debug(device.char_read("0000ffe1-0000-1000-8000-00805f9b34fb"))

        #cn.char_write(uuid, bytearray([1, 2]))
        #c = s.getCharacteristics(0xFFE1)[0]

        adapter.stop()


if __name__ == "__main__":
    unittest.main()
