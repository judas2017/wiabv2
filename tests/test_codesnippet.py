# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time


class CodeSnippetTests(unittest.TestCase):
    """Advanced test cases."""

    def test_string(self):
        addr = "90:fd:9f:7b:85:35"
        print(addr)
        addr = addr.upper()
        print(addr)


if __name__ == "__main__":
    unittest.main()
