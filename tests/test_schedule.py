
# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from src.lib.config import ConfigManager
import schedule
import threading

from src.sensor.thunderboard import Thunderboard
from src.controller.control_plant_light import ControlLights
from src.lib.bluetooth import BluetoothController
from src.controller.water_plant import WaterPlantController




class ScheduleTest(unittest.TestCase):
    """Advanced test cases."""

    def test_schedule(self):
        evtctr = 0
        val = ConfigManager().get_config_data("scheduler")
        for each in val:
            evtctr += 1
            if "frequency" in each["type"]:
                print(
                    "Setting type %s, frequency %s for class %s and method %s with threading = %s",
                    each["type"], each["frequency"], each["class"], each["method"],
                    each["threaded"])
                if each["threaded"] is True:
                    schedule.every(each["frequency"]).seconds.do(
                        SystemThreadingWrapper,
                        getattr(globals()[each["class"]](), each["method"]))
                else:
                    schedule.every(each["frequency"]).seconds.do(
                        getattr(globals()[each["class"]](), each["method"]))
            if "daytime" in each["type"]:
                print(
                    "Setting type %s, start time %s delivery secs %s deviceaddr %s for class %s and method %s with threading = %s",
                    each["type"], each["starttime"], each["deliverysecs"],
                    each["deviceaddr"], each["class"], each["method"],
                    each["threaded"])
                schedule.every().day.at(str(each["starttime"])).do(
                    getattr(globals()[each["class"]](), each["method"]),
                    deliverysecs=each["deliverysecs"],
                    deviceaddr=each["deviceaddr"],
                    relayno=each["relayno"])
        print("Loadded %s events into the scheduler", evtctr)


def SystemThreadingWrapper(job_func):
    job_thread = threading.Thread(target=job_func)
    job_thread.start()

if __name__ == "__main__":
    unittest.main()
