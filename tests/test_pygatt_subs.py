# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time

import pygatt
from binascii import hexlify
adapter = pygatt.GATTToolBackend()


class PyGattSubscriptionTest(unittest.TestCase):
    """Advanced test cases."""

    def handle_data(self, handle, value):
        """
        handle -- integer, characteristic read handle the data was received on
        value -- bytearray, the data returned in the notification
        """
        print("Received data: %s" % hexlify(value))

    def test_startsubs(self):
        try:
            adapter.start()
            device = adapter.connect('00:0B:57:1A:B7:DE', 10)

            device.subscribe(
                "c8546913-bfd9-45eb-8dde-9f8754f4a32e",
                callback=self.handle_data)
            time.sleep(100)
        finally:
            adapter.stop()


if __name__ == "__main__":
    unittest.main()
