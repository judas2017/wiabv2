# -*- coding: utf-8 -*-

import os
import json
import traceback
import unittest
import logging
import logging.config


class LoggerTest(unittest.TestCase):
    """Advanced test cases."""

    def test_get_debug_logger(self):
        logger = logging.getLogger(__name__)
        logger.debug("this is a debug message")
        logger = logging.getLogger(__name__)
        logger.debug("this is a debug message to _name_")

    def test_logger_behavior(self):
        mydir = os.path.dirname(__file__)
        path = os.path.join(mydir, "../src/config/logger.json")
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)

        logger = logging.getLogger(__name__)
        logger.debug("this is the debug message")
        logger.info("this is the info message")
        logger.warn("this is the warn message")
        logger.error("this is the error message")

        #logger = logging.getLogger(__name__)
        #logger.error('error to root looger')
        #logger.debug("this is a debug to root test, should not show")


if __name__ == "__main__":
    unittest.main()
