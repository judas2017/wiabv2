# main_with_ini.py
#import configparser
# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import os
#sys.path.append('../')
import traceback
import unittest
import datetime
import time
import json
from src.lib.config import ConfigManager as config


class ConfigReaderTest(unittest.TestCase):
    """Advanced test cases."""
    cfg = config()

    def test_read_config_file(self):
        mydir = os.path.dirname(__file__)
        path = os.path.join(mydir, "../src/config/serverconfig.json")
        with open(path, 'r') as f:
            config = json.load(f)
        print("Platform from config file: %s", config['platform'])

    def test_get_config_data(self):
        val = self.cfg.get_config_data("thunderboard_sensors")
        for each in val:
            print(each)

    def test_add_config_data(self):
        print("starting")
        val = self.cfg.get_config_data("thunderboard_sensors")
        newitem = {
            "addr": "00:0b:57:1a:b7:xx",
            "addrtype": "public",
            "tentid": 3
        }
        val.append(newitem)
        self.cfg.update_config_data()

    def test_update_config_data(self):
        print("starting")
        val = self.cfg.get_config_data("thunderboard_sensors")
        val[0]["tentid"] = 2
        self.cfg.update_config_data()

    def x_test_section_update(self):
        #
        # test harness to prove the underlying behavior of the core file and json libraries
        #
        CONFIGFILE = "serverconfig.json"
        cfgreader = dict()
        cfgpath = ""
        mydir = os.path.dirname(__file__)
        cfgpath = os.path.join(mydir, "../src/config/", CONFIGFILE)
        with open(cfgpath, 'r') as f:
            cfgreader = json.load(f)
            f.close()
        data = cfgreader["bluetooth_devices"]

        newitem = {
            "addr": "xx:xx:xx:xx",
            "addrtype": "public",
            "devicetype": "Thunder Sense",
            "tentid": 0
        }
        data.append(newitem)

        with open(cfgpath, "w") as f:
            #json.dump(self.cfgreader, f, sort_keys=True)
            json.dump(cfgreader, f, sort_keys=True)
            f.close()

        print(data)


if __name__ == "__main__":
    unittest.main()
