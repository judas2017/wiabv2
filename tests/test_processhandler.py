# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from multiprocessing import Process


def f(name):
        print 'hello', name
        time.sleep(30)

class ProcessHandererTest(unittest.TestCase):
    """Advanced test cases."""
    def test_processfork(self):
        print("entering test process fork")
        p = Process(target=f, args=('bob',))
        p.start()
        p.join(5)
        print('just said hello')
        


if __name__ == "__main__":
    unittest.main()
