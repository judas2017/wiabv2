
# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from src.lib.config import ConfigManager
from src.controller.water_plant import WaterPlantController
import schedule
import multiprocessing



def SingleSystemThreadingWrapper(job_func, args):
        # this is purposly using multiprocessing instead of threading due to
        # rsource leak problems w/the wemo ouimux library, anon_inode:[eventpoll] under process file open
        print (job_func, args)
        print ("addr %s" % args["deviceaddr"])
        job_thread = multiprocessing.Process(target=job_func, kwargs=args)
        job_thread.start()
        job_thread.join(90)

class ScheduleTest(unittest.TestCase):
    """Advanced test cases."""

    def test_schedule(self):
        args = {}
        args["deliverysecs"] = 1
        args["deviceaddr"] = "4C:3F:D3:02:B8:F2"
        args["relayno"] = 1

        schedule.every().day.at(str("13:45")).do(
        SingleSystemThreadingWrapper, WaterPlantController().waterplant, args)

        schedule.run_all()
    

if __name__ == "__main__":
    unittest.main()
 