# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from src.controller.control_plant_light import ControlLights as lights

#import src.lib.thunderboard as tb
#import src.lib.devicelist as dl
#import devicelist


class ControlLightTest(unittest.TestCase):
    """Advanced test cases."""

    def test_control_light_on(self):
        lt = lights()
        lt.control_lights()

    def test_control_light_off(self):
        lt = lights()
        lt.control_lights()

    def test_control_light(self):
        lt = lights()
        lt.control_lights()

    def test_isbetween(self):
        lt = lights()
        self.assertTrue(lt.is_between("01:00", "01:00", "02:00"))
        self.assertTrue(lt.is_between("02:00", "01:00", "02:00"))
        self.assertFalse(lt.is_between("02:01", "01:00", "02:00"))
        self.assertFalse(lt.is_between("02:01", "01:00", "02:00"))
        self.assertTrue(lt.is_between("12:01", "11:00", "01:00"))
        #does not work
        #self.assertTrue(lt.is_between("12:01PM", "01:00AM", "01:00PM"))
        self.assertTrue(lt.is_between("12:01", "01:00", "13:00"))
        #does not work
        self.assertTrue(lt.is_between("13:01", "13:00", "02:00"))
        self.assertTrue(lt.is_between("01:00", "13:00", "02:00"))
        self.assertTrue(lt.is_between("14:00", "13:00", "02:00"))
        self.assertFalse(lt.is_between("03:00", "13:00", "02:00"))


if __name__ == "__main__":
    unittest.main()
