# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
import src.server.server as server


class ServerTest(unittest.TestCase):
    """Advanced test cases."""

    def test_initialize_server(self):
        server.initialize_scheduler()


if __name__ == "__main__":
    unittest.main()
