# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import traceback
import unittest
import json
import datetime
import time
from time import sleep
from bluepy.btle import *
from src.sensor.tbdata import ThunderboardDOOLD
from src.lib.config import ConfigManager as config

# for timezone() 
import pytz 


class ThunderboardTest(unittest.TestCase):
    """Advanced test cases."""
    cfg = config()

    def test_getThunderboards(self):
        scanner = Scanner(0)
        devices = scanner.scan(30)
        tbsense = dict()
        for dev in devices:
            scanData = dev.getScanData()
            for (adtype, desc, value) in scanData:
                if desc == 'Complete Local Name':
                    if 'Thunder Sense #' in value:
                        deviceId = int(value.split('#')[-1])
                        print (adtype, desc, value)
                        self.get_sensor_data(dev.addr)
                        #self.get_sensor_data("90:FD:9F:7B:85:35")


    
    def get_sensor_data(self, addr):
        try:
            print("connect to addr %s" % addr)
            ble_service = Peripheral(addr)

            #ble_service.connect(addr, addrType)
            print("getting characteristics")
            characteristics = ble_service.getCharacteristics()
            print("loading thunderboard data object")
            tb = ThunderboardDOOLD(characteristics)

            text = ""
            text += "\n" + addr + "\n"
            now = datetime.datetime.now(pytz.timezone(self.cfg.get_config_data("general")["timezone"]))
            text += now.strftime("%Y-%m-%d %H:%M") + "\n"
            data = dict()
            data["dateTime"] = now
            data["name"] = ("thunderboard-%s" % addr.replace(':', ''))
            data["addr"] = addr

            for key in tb.char.keys():
                if key == "temperature":
                    data["temperature"] = tb.read_temperature()
                    text += "Temperature:\t{} C\n".format(data["temperature"])

                elif key == "humidity":
                    data["humidity"] = tb.read_humidity()
                    text += "Humidity:\t{} %RH\n".format(data["humidity"])

                elif key == "ambientLight":
                    data["ambientLight"] = tb.read_ambient_light()
                    text += "Ambient Light:\t{} Lux\n".format(
                        data["ambientLight"])

                elif key == "uvIndex":
                    data["uvIndex"] = tb.read_uv_index()
                    text += "UV Index:\t{}\n".format(data["uvIndex"])

                elif key == "co2":  # and tb.coinCell == False:
                    data["co2"] = tb.read_co2()
                    text += "eCO2:\t\t{}\n".format(data["co2"])

                elif key == "voc":  # and tb.coinCell == False:
                    data["voc"] = tb.read_voc()
                    text += "tVOC:\t\t{}\n".format(data["voc"])

                elif key == "sound":
                    data["sound"] = tb.read_sound()
                    text += "Sound Level:\t{}\n".format(data["sound"])

                elif key == "pressure":
                    data["pressure"] = tb.read_pressure()
                    text += "Pressure:\t{}\n".format(data["pressure"])
        finally:
            try:
                ble_service.disconnect()
            except Exception as e:
                print(
                    'Exception caught trying to disconnect, most likely device not found :%s '
                    % (e))

        print(text)
        return data

if __name__ == "__main__":
    unittest.main()
