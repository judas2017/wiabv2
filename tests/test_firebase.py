# -*- coding: utf-8 -*-
from os import sys, path
sys.path.append('../')

import traceback
import unittest
import datetime
import time
from src.lib.firebaseds import FirebaseDS as firebase


class FirebaseTest(unittest.TestCase):
    """Advanced test cases."""
    fb = firebase()

    def test_updateCollection(self):
        val = {"key": u"this is a test"}
        self.fb.update_collection("unittest", None, val)

    def test_update_sub_Collection(self):
        val = {"key": u"this is a test"}

        self.fb.update_sub_collection("unittest", "thunderboard456",
                                      "readings", val)

    def test_get_docref(self):
        import os
        import firebase_admin
        from firebase_admin import credentials
        from firebase_admin import firestore
        from firebase_admin import db as dbref

        mydir = os.path.dirname(__file__)
        new_path = os.path.join(mydir, "../src/google/",
                                "googleServiceAccountKey.json")
        # cred = credentials.Certificate('googleServiceAccountKey.json')
        cred = credentials.Certificate(new_path)
        # Initialize the app with a service account, granting admin privileges
        firebase_admin.initialize_app(
            cred, {"databaseURL": u"https://wiab-fcb26.firebaseio.com"},
            name="WIAB")
        # As an admin, the app has access to read and write all data, regradless of Security Rules
        db = firestore.client()
        doc = db.collection("thunderboards").document(
            "thunderboard-000B571AB7DE").collection("readings").order_by(
                "dateTime", direction="DESCENDING").limit(1).get()
        for x in doc:
            print(u'Document data: {}'.format(x.to_dict()))
            #print(x(u"dateTime"))

        #order_by(
        #    "dateTime").limit(1).get()


if __name__ == "__main__":
    unittest.main()
