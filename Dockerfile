# Python support can be specified down to the minor or micro version
# (e.g. 3.6 or 3.6.3).
# OS Support also exists for jessie & stretch (slim and full).
# See https://hub.docker.com/r/library/python/ for all supported Python
# tags from Docker Hub.

FROM balenalib/raspberrypi3-debian-python:2
#FROM balenalib/raspberrypi3-python:2
# following is recommended by Balena support - tbd
ENV DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

# set docker label and version information 
LABEL Name=wiab Version=0.1.0 MAINTAINER="Jeff Richard <jeff@talontechnology.com>"

# install compilers 
RUN apt-get update
#RUN apt-get install -y build-essential libglib2.0-dev nano 
RUN apt-get install -y build-essential libglib2.0-dev nano 

# work directory in the docker container where the code is deployed
WORKDIR /usr/src/wiab

# copy local requirements.txt to the target root
COPY requirements.txt ./

# install dependent packages
#RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -r requirements.txt

# copy source code bundlesd to target container
COPY . .
COPY patches/usr /usr

# start the wiab server process
CMD [ "python", "./start.py" ]