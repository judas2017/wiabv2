The layout in this package is based on the recommondations found here: https://realpython.com/python-application-layouts/
and this reference https://docs.python-guide.org/writing/structure/

Conda used to create environment - 
conda create -n wiabv2 python=3
source activate wiabv2

Pip to create requirements.txt file - 
pip3 freeze >requirements.txt

install pythong 3.7.1 
brew install python3.7.1

Instlal c-compiler for bluetooth 
https://cdn-learn.adafruit.com/downloads/pdf/bluefruit-le-python-library.pdf

Python Bluetooth cross platform library
https://github.com/pybluez/pybluez
pip install --upgrade git+https://github.com/karulis/pybluez.git#egg=pybluez
