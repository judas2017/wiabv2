import os
import logging
import logging.config
import json

mydir = os.path.dirname(__file__)
path = os.path.join(mydir, "../src/config/logger.json")

try:
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=logging.INFO)
finally:
    if f is not None:
        f.close()
