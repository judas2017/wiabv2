#!/usr/bin/python
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import logging
import time
import traceback
from src.lib.config import ConfigManager as config
from src.controller.relay import RelayController as RelayController

SECSINHOUR = 3600
VERSION = "1.0.0"


class WaterPlantController():
    logger = logging.getLogger(__name__)
    relay = RelayController()

    def waterplant(self, **kwargs):
        # deliverysecs=180, deviceaddr="", relayno=1
        self.logger.debug("addr passed: %s" % kwargs["deviceaddr"]) 
        self.logger.debug("relayno: %s" % str(kwargs["relayno"]))
        self.logger.debug("deliverysecs: %s" % kwargs["deliverysecs"])
        try:
            self.logger.info("water relay being turned on")
            self.relay.write_sensor_data(kwargs["deviceaddr"], kwargs["relayno"], "on")
            time.sleep(kwargs["deliverysecs"])
        except Exception as e:
            self.logger.exception(e)
        finally:
            self.logger.info("water relay being turned off")
            self.relay.write_sensor_data(kwargs["deviceaddr"], kwargs["relayno"], "off")