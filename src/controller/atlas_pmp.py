#!/usr/bin/python
# pull data from light sensor

import time
import traceback
from datetime import datetime
import logging
from src.lib.config import ConfigManager as config
from src.sensor.atlas_i2c import Atlas_I2C as atlas_i2c
from retrying import retry
# for timezone()
import pytz

SECSINHOUR = 3600
VERSION = "1.0.0"
CONFIGSECTION = "atlas_i2c_pmp"


class ControlAtlasPMP:
    logger = logging.getLogger(__name__)
    cfg = config()


    def control_lights(self):
        pmp = atlas_i2c()

        val = config().get_config_data(CONFIGSECTION)
        for each in val:
            self.logger.debug(
                "Found device in active state of {}".format(each["isactive"]))
        if each["isactive"] is True:
            self.logger.debug(
                "Setting device address {}".format(each["sensoraddr"]))
            self.set_i2c_address(each["sensoraddr"])
            self.logger.debug(
                "Writting data to sensor {}".format(each["name"]))
            each["dateTime"] = datetime.datetime.now(
                pytz.timezone('US/Eastern'))
            self.logger.debug(
                "starting write cycle {}".format(each["dateTime"]))
            try:
                maxreadings = 5
                sensor_reading_total = 0
                accuracy = 2
                for x in range(0, maxreadings):
                    if "ec" in each["sensortype"]:
                        sensor_reading = round(float(self.query("R")) *
                                               each["ppm_multiplier"], accuracy)
                    elif "temp" in each["sensortype"]:
                        sensor_reading = round(
                            (float(self.query("R")) * 1.8) + 32, accuracy)
                    else:
                        sensor_reading = round(
                            float(self.query("R")), accuracy)
                    sensor_reading_total = sensor_reading_total + sensor_reading
                sensor_reading_total = round(
                    sensor_reading_total / maxreadings, accuracy)
                each["reading"] = sensor_reading_total
                self.fb.update_sub_collection(
                    "atlasscientific", each["sensortype"] +
                    "-" + str(each["sensoraddr"]),
                    "readings", each)
            except Exception as e:
                self.logger.exception('Exception caught :%s ' % (e))


            light_should_be_on = False
            houron = each["houron"]
            houroff = each["houroff"]
            switchname = each["switchname"]
            self.logger.debug(
                "controll ligth name %s, hour-on %s, hour-off %s", switchname, houron, houroff)
            now = datetime.now(pytz.timezone(
                self.cfg.get_config_data("general")["timezone"])).strftime("%H:%M")
            light_should_be_on = self.is_between(now, houron, houroff)
            self.logger.debug("Light on should be %s, controlling light name %s, hour-on: %s, hour-off:%s, now:%s",
                              light_should_be_on, switchname, houron, houroff, now)
            try:
                wm.set_wemo_state(switchname, light_should_be_on)
            except Exception as e:
                self.logger.exception(
                    "failed calling set wemo state retrying 5 times, exception %s raised" % (e))
        return

    def is_between(self, check_time, begin_time, end_time):
        isTime = False
        # If check time is not given, default to current UTC time
        check_time = check_time or datetime.utcnow().time()
        self.logger.debug(
            "check_time is %s, begin_time is %s, end_time is %s", check_time, begin_time, end_time)

        if begin_time < end_time:
            isTime = check_time >= begin_time and check_time <= end_time
        else:  # crosses midnight
            isTime = check_time >= begin_time or check_time <= end_time
        self.logger.debug("is time withing period returned %s", isTime)
        return isTime
