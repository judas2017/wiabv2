#!/usr/bin/python
# pull data from light sensor

import time
import traceback
from datetime import datetime
import logging
from src.lib.config import ConfigManager as config
from src.controller.wemo import WemoController as wemo
from retrying import retry
# for timezone() 
import pytz 

SECSINHOUR = 3600
VERSION = "1.0.0"


class ControlLights:
    logger = logging.getLogger(__name__)

    def control_lights(self):
        wm = wemo()
        cfg = config()
        val = cfg.get_config_data("wemo_controllers")
        for each in val:
            self.logger.debug("Found light %s isactive value is %s", each["switchname"], each["isactive"])
            if each["isactive"] is True:
                light_should_be_on = False
                houron = each["houron"]
                houroff = each["houroff"]
                switchname = each["switchname"]
                self.logger.debug("controll ligth name %s, hour-on %s, hour-off %s", switchname,houron, houroff)
                self.logger.debug("Timezone is set to %s" % cfg.get_config_data("general")["timezone"])
                now = datetime.now(pytz.timezone(cfg.get_config_data("general")["timezone"])).strftime("%H:%M")
                light_should_be_on = self.is_between(now, houron, houroff)
                self.logger.debug("Light on should be %s, controlling light name %s, hour-on: %s, hour-off:%s, now:%s",
                    light_should_be_on, switchname, houron, houroff, now)
                try:
                    wm.set_wemo_state(switchname, light_should_be_on)
                except Exception as e:
                    self.logger.exception("failed calling set wemo state retrying 5 times, exception %s raised"% (e))
        return

    def is_between(self, check_time, begin_time, end_time):
        isTime = False
        # If check time is not given, default to current UTC time
        check_time = check_time or datetime.utcnow().time()
        self.logger.debug("check_time is %s, begin_time is %s, end_time is %s", check_time, begin_time, end_time)

        if begin_time < end_time:
            isTime = check_time >= begin_time and check_time <= end_time
        else:  # crosses midnight
            isTime = check_time >= begin_time or check_time <= end_time
        self.logger.debug("is time withing period returned %s", isTime)
        return isTime
