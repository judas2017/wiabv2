#!/usr/bin/env python
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import logging
import time
import traceback
from retrying import retry
from ouimeaux.environment import Environment

VERSION = "1.0.0"
SECS = 30


class WemoController(object):
    logger = logging.getLogger(__name__)
    env = Environment(with_discovery=True)

    def __init__(self):
        # environment startup must be done outside of method using env. or socket connection leak
        self.env.start()
        self.logger.debug("starting wemo discovery for %s" % SECS)
        self.env.discover(SECS)  # default discovery wait time in secs

    @retry(stop_max_attempt_number=3, wait_fixed=2000)
    def set_wemo_state(self, switchname, desiredstate):
        try:
            self.logger.debug("switches found: %s", self.env.list_switches())
            wswitch = self.env.get_switch(switchname)
            self.logger.debug("Switch state is: %s and status should be %s",
                              wswitch.get_state(), desiredstate)
            if desiredstate != wswitch.get_state():
                if desiredstate is True:
                    self.logger.info(
                        "Light is currently off, turning light on")
                    wswitch.on()
                else:
                    self.logger.info(
                        "Light is currently on, turning light off")
                    wswitch.off()
        except Exception as e:
            self.logger.exception(
                'Exception caught :%s retrying upto 2 times ' % (e))
            self.logger.debug(
                "Running env.discovery again as result of exception")
            self.env.discover(SECS)
            raise
        finally:
            self.logger.debug("exiting wemo controller class")
        return True
