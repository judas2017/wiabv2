import struct
from time import sleep
import datetime
import logging
import binascii
#import pygatt
from bluepy.btle import *
from src.lib.config import ConfigManager as config
from retrying import retry


class RelayController:
    logger = logging.getLogger(__name__)
    CHANNELS = {
        "1on": "A00101A2",
        "1off": "A00100A1",
        "2on": "A00201A3",
        "2off": "A00200A2",
        "3on": "A00301A4",
        "3off": "A00300A3",
        "4on": "A00401A5",
        "4off": "A00400A4"
    }

    # characteristic that controls the relay switch toggles
    DSD_RELAY_CHAR = "0000ffe1-0000-1000-8000-00805f9b34fb"

    def __init__(self):
        self.char = dict()
        self.logger.debug("init method called")

    @retry(stop_max_attempt_number=15, wait_fixed=5000)
    def write_sensor_data(self, addr, channel=1, onoff="off"):
        try:
            self.logger.debug("Connecting to dsd relay %s" % addr)
            p = Peripheral(addr)
            self.logger.debug("connected to relay")
            code = self.CHANNELS[str(channel) + onoff]
            self.logger.debug("writting to the characteristic")
            s = p.getServiceByUUID(UUID(0xFFE0))
            c = s.getCharacteristics(0xFFE1)[0]
            asc = binascii.a2b_hex(code)
            c.write(asc)
        except Exception as e:
            self.logger.exception('Exception caught, retrying 5 times :%s ' %
                                  (e))
            raise
        finally:
            self.logger.debug("Closing connection to periferal")
            try:
                if p is not None: p.disconnect()
            except Exception as e:
                self.logger.exception(
                    "Exception caught trying to disconnect peripheral %s" %
                    (e))
                raise

    def close_all_relays(self):
        #this method is intended to be called at shutdown of the system
        self.logger.debug("closing all relays called")
        devs = config().get_config_data("scheduler")
        self.logger.debug("Getting relay devices")
        for each in devs:
            if "WaterPlantController" in each["class"]:
                addr = each["deviceaddr"]
                relayno = each["relayno"]
                self.logger.debug(
                    "WaterPlantController found addr: %s, relayno: %s", addr,
                    relayno)
                self.write_sensor_data(addr, relayno, "off")