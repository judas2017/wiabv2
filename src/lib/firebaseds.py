import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import logging
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import db as dbref
from retrying import retry
from datetime import date

VERSION = "1.0.0"


class FirebaseDS(object):
    __shared_state = {}
    db = firestore.client
    isInitialized = False
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.__dict__ = self.__shared_state
        if self.isInitialized is False:
            self.logger.debug("initalizing firebase_admin app")
            # Fetch the service account key JSON file contents
            mydir = os.path.dirname(__file__)
            new_path = os.path.join(mydir, "../google/",
                                    "googleServiceAccountKey.json")
            # cred = credentials.Certificate('googleServiceAccountKey.json')
            cred = credentials.Certificate(new_path)
            # Initialize the app with a service account, granting admin privileges
            firebase_admin.initialize_app(
                cred, {"databaseURL": "https://wiab-fcb26.firebaseio.com"})
            # As an admin, the app has access to read and write all data, regradless of Security Rules
            self.db = firestore.client()
            self.isInitialized = True

    @retry(stop_max_attempt_number=3, wait_fixed=500)
    def update_doc(self, collection, documentId, data):
        self.logger.debug(
            "update_doc called with collection %s, documentId %s, data %s",
            collection, documentId, data)
        if documentId is not None:
            self.db.collection(collection).document(documentId).set(data)
        else:
            self.db.collection(collection).document().set(data)

    @retry(stop_max_attempt_number=3, wait_fixed=500)
    def update_collection(self, collection, documentId, data):
        self.logger.debug(
            "update_collection called with %s collection, %s documentId, %s data",
            collection, documentId, data)
        if documentId is not None:
            self.db.collection(collection).document(documentId).set(data)
        else:
            self.db.collection(collection).document().set(data)

    @retry(stop_max_attempt_number=3, wait_fixed=500)
    def update_sub_collection(self, collection, document, sub_collection,
                              sub_document):
        self.logger.debug(
            "update subcollection called collection = %s, document = %s, sub_collection = %s, sub_document = %s",
            collection, document, sub_collection, sub_document)

        ref = self.db.collection(collection).document(document)
        doc_ref = ref.collection(sub_collection).document().set(sub_document)
        self.logger.debug("update completed")

    @retry(stop_max_attempt_number=3, wait_fixed=500)
    def get_doc_ref(self, collid, docid):
        self.logger.debug("get_doc_ref called with %s collid, docid %s",
                          collid, docid)
        doc_ref = self.db.collection(collid).document(docid)
        return doc_ref

    @retry(stop_max_attempt_number=3, wait_fixed=500)
    def get_col_ref(self, collection):
        self.logger.debug("get_col_ref called with %s collection", collection)
        ref = self.db.collection(collection)
        return ref

    def update_aggregation(self, collid, docid):
        # gets the aggregate document from the device aggregate collection where doc is the reading type
        self.logger.debug("update aggregation called with docid %s collid %s",
                          docid, collid)
        docRef = self.get_doc_ref(collid, docid)

        # if null check here

        # compute new average calculations here - per tracked data element
        today = date.today()

        today = docRef.data().today

        data = dict()
        data["dateTime"] = "now"

        # update the docRef w/the new values
        self.update_doc(collid, docid, data)
        return