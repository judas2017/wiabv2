import os
import logging
import json
from retrying import retry

CONFIGFILE = "serverconfig.json"
CONFIGFILE_BALENA = "/data/config/serverconfig.json"


class ConfigManager(object):
    logger = logging.getLogger(__name__)
    cfgreader = dict()
    cfgpath = ""

    def __init__(self):
        if len(self.cfgreader) is 0:
            self.logger.debug("loading config file for first time")
            if os.path.exists(CONFIGFILE_BALENA) is True:
                self.cfgpath = (CONFIGFILE_BALENA)
            else:
                self.logger.debug("no configuration file found at %s" % CONFIGFILE_BALENA)
                mydir = os.path.dirname(__file__)
                self.cfgpath = os.path.join(mydir, "../config/", CONFIGFILE)
            try:
                self.logger.info("loading configuration file from: %s" % self.cfgpath)
                with open(self.cfgpath, 'r') as f:
                    self.cfgreader = json.load(f)
            finally:
                f.close()

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def get_config_data(self, key):
        self.logger.debug("entering get_config_data with key: %s" % key)
        return self.cfgreader[key]

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def update_config_data(self, section, newitem):
        try:
            self.logger.debug("getting section: %s" % section)
            secdata = self.get_config_data(section)
            self.logger.debug("updating with new item %s" % newitem)
            secdata.append(newitem)
            self.logger.debug("updating configuration file, opening for write")
            with open(self.cfgpath, "w") as f:
                #json.dump(self.cfgreader, f, sort_keys=True)
                json.dump(self.cfgreader, f, sort_keys=True)
        finally:
            f.close()
            self.logger.debug("configuration file update was sucessfull")
