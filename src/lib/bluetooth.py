import struct
from time import sleep
import datetime
import logging
#import pygatt
from bluepy.btle import *
from src.lib.config import ConfigManager as config
from src.lib.firebaseds import FirebaseDS as firebase
from retrying import retry

BLETIMEOUT = 10
BTDEVICECONFIGSECTION = "bluetooth_devices"


class BluetoothController:
    logger = logging.getLogger(__name__)
    fb = firebase()
    is_scanning = False

    #connections = {}

    def __init__(self):
        self.char = dict()
        self.logger.debug("init method called")

    @retry(stop_max_attempt_number=2, wait_fixed=2000)
    def scan_for_devices(self, duration=15):
        board_found = False
        self.is_scanning = True
        self.logger.debug("Scanning for duration %s seconds...." % duration)
        try:
            scanner = Scanner(0)
            devices = scanner.scan(duration)
            self.logger.debug("Scanning complete")
            devices = scanner.getDevices()

            for dev in devices:
                scanData = dev.getScanData()
                dev.addr = dev.addr.upper()
                for (adtype, desc, value) in scanData:
                    if desc == "Complete Local Name":
                        if self.check_known_device_types(value) is not None:
                            self.logger.debug(
                                "Found baord at addr %s of type: %s", dev.addr,
                                value)
                            self.update_known_devices(
                                dev.addr, dev.addrType,
                                self.check_known_device_types(value), 0)
                            board_found = True

        except Exception as e:
            self.logger.exception('Exception caught :%s ' % (e))
            raise
        finally:
            self.is_scanning = False
            self.logger.debug("exiting scan_for_devices")

        return board_found

    def check_known_device_types(self, deviceType):
        if "Thunder Sense" in deviceType:
            return "Thunder Sense"
        if "DSD Relay" in deviceType:
            return "DSD Relay"
        else:
            return None

    def update_known_devices(self, addr, addrtype, devicetype, tentid=0):
        self.logger.debug("Enter update_known_devices method")
        found = False
        cfg = config()
        boards = cfg.get_config_data(BTDEVICECONFIGSECTION)
        self.logger.debug("loaded %s boards" % len(boards))
        for each in boards:
            if addr in each["addr"]:
                found = True
                self.logger.debug("board found in config, not adding")
        if not found:
            self.logger.debug(
                "New board found, updating configuration file with addr %s",
                addr)
            newitem = {
                "addr": addr,
                "addrtype": addrtype,
                "devicetype": devicetype,
                "tentid": tentid
            }
            #boards.append(newitem)
            self.logger.debug("about to update configuration file")
            cfg.update_config_data(BTDEVICECONFIGSECTION, newitem)
        else:
            self.logger.debug(
                "No new boards found, already exists in configuration file")
