#!/usr/bin/python
# Server process that consumes sensor readings and transmitts them to the core IoT engine via MQTTF
#
import os
import sys
# append the top level director to the python path, needed so imports work correctly
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
import logging
import time
import signal
import multiprocessing
import schedule
import inspect
from src.lib.config import ConfigManager
from src.sensor.thunderboard import Thunderboard
from src.controller.water_plant import WaterPlantController
from src.controller.control_plant_light import ControlLights
from src.lib.bluetooth import BluetoothController
from src.controller.relay import RelayController
from src.sensor.atlas_i2c import Atlas_I2C

VERSION = "0.0.1"
POLLINT = 10
run = True
logger = logging.getLogger(__name__)


def SystemThreadingWrapper(job_func, args):
    # this is purposly using multiprocessing instead of threading due to
    # rsource leak problems w/the wemo ouimux library, anon_inode:[eventpoll] under process file open
    # if there are more than 1 parameter pass the parm dict obj
    if len(args) > 1 : 
        job_thread = multiprocessing.Process(target=job_func, kwargs=args)
    else: 
        job_thread = multiprocessing.Process(target=job_func)
    job_thread.start()
    if args["threaded"] is False : 
        job_thread.join(90)

def initialize_scheduler():
    evtctr = 0
    logger.debug("Initializing scheduler")
    val = ConfigManager().get_config_data("scheduler")
    logger.debug("configuration data loaded for scheduler %s", val)
    for each in val:
        logger.debug("conifguration loaded %s", each)
        if each["isactive"] is True:
            evtctr += 1
            args = {}
            args["threaded"] = each["threaded"]
            if "frequency" in each["type"]:
                logger.debug(
                    "Setting type %s, frequency %s for class %s and method %s with threading = %s",
                    each["type"], each["frequency"], each["class"], each["method"],
                    each["threaded"])
                schedule.every(each["frequency"]).seconds.do(
                    SystemThreadingWrapper,
                    getattr(globals()[each["class"]](), each["method"]), args)
            if "daytime" in each["type"]:
                logger.debug(
                    "Setting type %s, start time %s delivery secs %s deviceaddr %s for class %s and method %s with threading = %s",
                    each["type"], each["starttime"], each["deliverysecs"],
                    each["deviceaddr"], each["class"], each["method"],
                    each["threaded"])
                args["deliverysecs"] = each["deliverysecs"]
                args["deviceaddr"] = each["deviceaddr"]
                args["relayno"] = each["relayno"]
                schedule.every().day.at(str(each["starttime"])).do(
                    SystemThreadingWrapper, getattr(globals()[each["class"]](), each["method"]), args)
        logger.debug("Loadded %s events into the scheduler", evtctr)


def handler_stop_signals(signum, frame):
    global run
    run = False


def main():
    x = 0 #counter to be used for logging
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    logger.info("server.py version %s starting", VERSION)
    initialize_scheduler()
    schedule.run_all(0)
    while run:
        try:
            x += 1
            # log every third cycle though
            if x%6 is 0 : logger.debug("checking for pending jobs")
            schedule.run_pending()
            time.sleep(POLLINT)
        except Exception as e:
            logger.exception('Exception caught from subsystem :%s ' % (e))
    else:
        logger.warn("System shutting down, SIGTERM issued")
        logger.info("Ensuring all relay switches are closed before shutdown")
        RelayController().close_all_relays()
        sys.exit(0)


if __name__ == '__main__':
    main()