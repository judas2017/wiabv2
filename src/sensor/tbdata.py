from bluepy.btle import *
import struct
from time import sleep
import logging
 

class ThunderboardDOOLD:
    logger = logging.getLogger(__name__)

    def __init__(self, characteristics):
        # self.dev = dev
        self.char = dict()
        self.name = ""
        self.coinCell = False
        self.logger.debug("Getting characteristics")
        for k in characteristics:
            if k.uuid == "2a6e":
                self.char["temperature"] = k

            elif k.uuid == "2a6f":
                self.char["humidity"] = k

            elif k.uuid == "2a76":
                self.char["uvIndex"] = k

            elif k.uuid == "2a6d":
                self.char["pressure"] = k

            elif k.uuid == "c8546913-bfd9-45eb-8dde-9f8754f4a32e":
                self.char["ambientLight"] = k

            elif k.uuid == "c8546913-bf02-45eb-8dde-9f8754f4a32e":
                self.char["sound"] = k

            elif k.uuid == "efd658ae-c401-ef33-76e7-91b00019103b":
                self.char["co2"] = k

            elif k.uuid == "efd658ae-c402-ef33-76e7-91b00019103b":
                self.char["voc"] = k

            elif k.uuid == "ec61a454-ed01-a5e8-b8f9-de9ec026ec51":
                self.char["power_source_type"] = k

    def read_temperature(self):
        value = self.char["temperature"].read()
        value = struct.unpack("<H", value)
        value = value[0] / 100
        # convert to fahrenheit
        value = value * 1.8 + 32
        return value

    def read_humidity(self):
        value = self.char["humidity"].read()
        value = struct.unpack("<H", value)
        value = value[0] / 100
        return value

    def read_ambient_light(self):
        value = self.char["ambientLight"].read()
        value = struct.unpack("<L", value)
        value = value[0] / 100
        return value

    def read_uv_index(self):
        value = self.char["uvIndex"].read()
        value = ord(value)
        return value

    def read_co2(self):
        value = self.char["co2"].read()
        value = struct.unpack("<h", value)
        value = value[0]
        return value

    def read_voc(self):
        value = self.char["voc"].read()
        value = struct.unpack("<h", value)
        value = value[0]
        return value

    def read_sound(self):
        value = self.char["sound"].read()
        value = struct.unpack("<h", value)
        value = value[0] / 100
        return value

    def read_pressure(self):
        value = self.char["pressure"].read()
        value = struct.unpack("<L", value)
        value = value[0] / 1000
        return value
