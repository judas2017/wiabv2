#!/usr/bin/env python

#  Import Python Modules

import io
import os
import sys
import fcntl
import logging
import datetime
# for timezone() 
import pytz 

from time import sleep

#  Import Custom Modules
from src.lib.firebaseds import FirebaseDS as firebase
from src.lib.config import ConfigManager as config
CONFIGSECTION = "atlas_i2c_sensors"


# Define Atlas Scientific Sensor Class
class Atlas_I2C:
    logger = logging.getLogger(__name__)
    fb = firebase()
    cfg = config()

    long_timeout = 1.5  # the timeout needed to query readings & calibrations
    short_timeout = .5  # timeout for regular commands
    default_bus = 1  # the default bus for I2C on the newer Raspberry Pis,
                     # certain older boards use bus 0
    default_address = 102  # the default address for the Temperature sensor

    def __init__(self, address=default_address, bus=default_bus):
        # open two file streams, one for reading and one for writing
        # the specific I2C channel is selected with the bus
        # it is usually 1, except for older revisions where its 0
        # wb and rb indicate binary read and write
        self.file_read = io.open("/dev/i2c-" + str(bus), "rb", buffering=0)
        self.file_write = io.open("/dev/i2c-" + str(bus), "wb", buffering=0)

        # initializes I2C to either a user specified or default address
        self.set_i2c_address(address)
        self.char = dict()
        self.logger.debug("init method called")

    def set_i2c_address(self, addr):
        # set the I2C communications to the slave specified by the address
        # The commands for I2C dev using the ioctl functions are specified in
        # the i2c-dev.h file from i2c-tools
        I2C_SLAVE = 0x703
        fcntl.ioctl(self.file_read, I2C_SLAVE, addr)
        fcntl.ioctl(self.file_write, I2C_SLAVE, addr)

    def write(self, string):
        # appends the null character and sends the string over I2C
        string += "\00"
        self.file_write.write(bytes(string)) #, 'UTF-8')

    # def read(self, num_of_bytes=31):
    #     # reads a specified number of bytes from I2C,
    #     # then parses and displays the result
    #     res = self.file_read.read(num_of_bytes)  # read from the board
    #     # remove the null characters to get the response
    #     response = list([x for x in res])

    #     if(response[0] == 1):  # if the response isnt an error
    #         # change MSB to 0 for all received characters except the first
    #         # and get a list of characters
    #         char_list = [chr(x & ~0x80) for x in list(response[1:])]
    #         # NOTE: having to change the MSB to 0 is a glitch in the
    #         # raspberry pi, and you shouldn't have to do this!
    #         # convert the char list to a string and returns it
    #         result = ''.join(char_list)
    #         return result.split('\x00')[0]
    #     else:
    #         return "Error: " + str(ord(response[0]))
    def get_device_info(self):
        _name = ""
        _module = ""
        if(_name == ""):
            return _module + " " + str(self.default_address)
        else:
            return _module + " " + str(self.default_address) + " " + _name
   
    def handle_raspi_glitch(self, response):
        '''
        Change MSB to 0 for all received characters except the first 
        and get a list of characters
        NOTE: having to change the MSB to 0 is a glitch in the raspberry pi, 
        and you shouldn't have to do this!
        '''
        if self.app_using_python_two():
            return list(map(lambda x: chr(ord(x) & ~0x80), list(response)))
        else:
            return list(map(lambda x: chr(x & ~0x80), list(response[1:])))

    def response_valid(self, response):
        valid = True
        error_code = None
        if(len(response) > 0):
            
            if self.app_using_python_two():
                error_code = str(ord(response[0]))
            else:
                error_code = str(response[0])
                
            if error_code != '1': #1:
                valid = False

        return valid, error_code
  
    def app_using_python_two(self):
        return sys.version_info[0] < 3

    def get_response(self, raw_data):
        if self.app_using_python_two():
            response = [i for i in raw_data if i != '\x00']
        else:
            response = raw_data

        return response

    def read(self, num_of_bytes=31):
        '''
        reads a specified number of bytes from I2C, then parses and displays the result
        '''
        
        raw_data = self.file_read.read(num_of_bytes)
        response = self.get_response(raw_data=raw_data)
        #print(response)
        is_valid, error_code = self.response_valid(response=response)

        if is_valid:
            char_list = self.handle_raspi_glitch(response[1:])
            result = str(''.join(char_list))
        else:
            result = error_code

        return float(result)


    def query(self, string):
        # write a command to the board, wait the correct timeout,
        # and read the response
        self.write(string)

        # the read and calibration commands require a longer timeout
        if((string.upper().startswith("R")) or
           (string.upper().startswith("CAL"))):
            sleep(self.long_timeout)
        elif((string.upper().startswith("SLEEP"))):
            return "sleep mode"
        else:
            sleep(self.short_timeout)
        return self.read()

    def close(self):
        self.file_read.close()
        self.file_write.close()


    # entry routine from schedule to fetch data and push update to firebase
    # The hex representation of the I2C addresses are shown. (dissolved oxygen = 0x61 / 97, 
    # pH = 0x63 / 99, temperature = 0x66 / 102, Embedded Conductivity=0x64 / 100)
    def update_firebase_atlasi2c(self):
        self.logger.debug("Entering update_firebase_atlasi2c method")
        try:
            boards = config().get_config_data(CONFIGSECTION)
            self.logger.debug("Getting data for Atlas Sensors")
            for each in boards:
                self.logger.debug("Found device in active state of {}".format(each["isactive"]))
                if each["isactive"] is True:
                    self.logger.debug("Setting device address {}".format(each["sensoraddr"]))
                    self.set_i2c_address(each["sensoraddr"])
                    self.logger.debug("Reading data from sensor {}".format(each["name"]))
                    each["dateTime"] = datetime.datetime.now(pytz.timezone(self.cfg.get_config_data("general")["timezone"]))
                    self.logger.debug("starting read cycle {}".format(each["dateTime"]))

                    try:
                        maxreadings = 5
                        sensor_reading_total = 0
                        accuracy = 2
                        for x in range(0, maxreadings):
                            if "ec" in each["sensortype"]:
                                sensor_reading = round(float(self.query("R")) *
                                    each["ppm_multiplier"], accuracy)
                            elif "temp" in each["sensortype"]:
                                sensor_reading = round((float(self.query("R")) * 1.8) + 32 , accuracy)
                            else:
                                sensor_reading = round(float(self.query("R")), accuracy)
                            sensor_reading_total = sensor_reading_total + sensor_reading
                        sensor_reading_total = round(sensor_reading_total / maxreadings, accuracy)
                        each["reading"] = sensor_reading_total
                        self.fb.update_sub_collection(
                            "atlasscientific", each["sensortype"] + "-" + str(each["sensoraddr"]), 
                            "readings", each)
                    except Exception as e:
                        self.logger.exception('Exception caught :%s ' % (e))
            
        except Exception as e:
            self.logger.exception('Exception caught :%s ' % (e))
       
        