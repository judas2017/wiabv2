from time import sleep
import datetime
import logging
#import pygatt
from bluepy.btle import *
from retrying import retry
from src.sensor.thunderboard_data import ThunderboardDO
from src.lib.config import ConfigManager as config
from src.lib.firebaseds import FirebaseDS as firebase
from src.lib.bluetooth import BluetoothController as btc

# for timezone() 
import pytz 

DISCOVERSECS = 25
CONFIGSECTION = "bluetooth_devices"


class Thunderboard:
    logger = logging.getLogger(__name__)
    fb = firebase()
    cfg = config()
    is_thread_updating = False

    def __init__(self):
        self.char = dict()
        self.logger.debug("init method called")

    def update_firebase_thunderdata(self):
        ctr = 0
        self.logger.debug("Entering update_firebase_thunderdata method")
        while self.is_thread_updating is True:
            self.logger.debug("Firebase ds update in progress, waiting 2s...")
            sleep(2)
        try:
            self.is_thread_updating = True
            boards = config().get_config_data(CONFIGSECTION)
            self.logger.debug("Getting data for Thunder Sense")
            for each in boards:
                self.logger.debug("Thunderboard config %s", each)
                if "Thunder Sense" in each["devicetype"] and each["isactive"] is True:
                    self.logger.debug("Reading sensor data for device %s",
                                      each["addr"])
                    try:
                        data = self.get_sensor_data(each["addr"])
                        self.fb.update_sub_collection(
                            "thunderboards", data["name"], "readings", data)
                    except Exception as ex:
                        self.logger.exception(
                            "exception caught tyring to get sensor data: %s" %
                            (ex))
                    ctr = +1
            if ctr is 0:
                self.logger.error(
                    "No active Thunderboards found in configuration file. run scan to discover boards."
                )
        except Exception as e:
            self.logger.exception('Exception caught :%s ' % (e))
        finally:
            self.is_thread_updating = False

    @retry(stop_max_attempt_number=25, wait_fixed=250)
    def get_sensor_data(self, addr):
        addr = addr.upper()
        self.logger.debug("Getting connection to %s" % addr)
        ble_service = Peripheral(addr)
        text = ""
        data = dict()
        try:
            text += "\n" + addr + "\n"
            now = datetime.datetime.now(pytz.timezone(self.cfg.get_config_data("general")["timezone"]))            
            text += now.strftime("%Y-%m-%d %H:%M") + "\n"
            data["dateTime"] = now
            data["name"] = ("thunderboard-%s" % addr.replace(':', ''))
            data["addr"] = addr
            self.logger.debug("getting characteristics")
            characteristics = ble_service.getCharacteristics()
            tb = ThunderboardDO(characteristics)

            for key in tb.char.keys():
                if key == "temperature":
                    data["temperature"] = tb.read_temperature()
                    text += "Temperature:\t{} C\n".format(data["temperature"])

                elif key == "humidity":
                    data["humidity"] = tb.read_humidity()
                    text += "Humidity:\t{} %RH\n".format(data["humidity"])

                elif key == "ambientLight":
                    data["ambientLight"] = tb.read_ambient_light()
                    text += "Ambient Light:\t{} Lux\n".format(
                        data["ambientLight"])

                elif key == "uvIndex":
                    data["uvIndex"] = tb.read_uv_index()
                    text += "UV Index:\t{}\n".format(data["uvIndex"])

                elif key == "co2":  # and tb.coinCell == False:
                    data["co2"] = tb.read_co2()
                    text += "eCO2:\t\t{}\n".format(data["co2"])

                elif key == "voc":  # and tb.coinCell == False:
                    data["voc"] = tb.read_voc()
                    text += "tVOC:\t\t{}\n".format(data["voc"])

                elif key == "sound":
                    data["sound"] = tb.read_sound()
                    text += "Sound Level:\t{}\n".format(data["sound"])

                elif key == "pressure":
                    data["pressure"] = tb.read_pressure()
                    text += "Pressure:\t{}\n".format(data["pressure"])
            self.logger.debug(text)

        except Exception as e:
            self.logger.warn(
                "error attempting to connect to adapter; %s" % (e))
            raise
        finally:
            self.logger.debug("disconnecting from the peripheral")
            ble_service.disconnect()

        return data
