import struct
import logging
from retrying import retry


class ThunderboardDO:
    logger = logging.getLogger(__name__)
    conn = object

    def __init__(self, characteristics):
        self.char = dict()
        self.name = ""
        self.coincell = False
        self.logger.debug("Getting characteristics")
        for k in characteristics:
            self.logger.debug("Char: %s" % k)
            if k.uuid == "2a6e":
                self.char["temperature"] = k
            elif k.uuid == "2a6f":
                self.char["humidity"] = k
            elif k.uuid == "2a76":
                self.char["uvIndex"] = k
            elif k.uuid == "2a6d":
                self.char["pressure"] = k
            elif k.uuid == "c8546913-bfd9-45eb-8dde-9f8754f4a32e":
                self.char["ambientLight"] = k
            elif k.uuid == "c8546913-bf02-45eb-8dde-9f8754f4a32e":
                self.char["sound"] = k
            elif k.uuid == "efd658ae-c401-ef33-76e7-91b00019103b":
                self.char["co2"] = k
            elif k.uuid == "efd658ae-c402-ef33-76e7-91b00019103b":
                self.char["voc"] = k
            elif k.uuid == "ec61a454-ed01-a5e8-b8f9-de9ec026ec51":
                self.char["power_source_type"] = k
        self.logger.debug("done reading characteristics")

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_temperature(self):
        self.logger.debug("reading temp characteristic")
        value = self.char["temperature"].read()
        value = struct.unpack("<H", value)
        value = value[0] / 100
        # convert to fahrenheit
        value = value * 1.8 + 32
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_humidity(self):
        self.logger.debug("reading humidity characteristic")
        value = self.char["humidity"].read()
        value = struct.unpack("<H", value)
        value = value[0] / 100
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_ambient_light(self):
        self.logger.debug("reading ambient light characteristic")
        value = self.char["ambientLight"].read()
        value = struct.unpack("<L", value)
        value = value[0] / 100
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_uv_index(self):
        self.logger.debug("reading UV Index characteristic")
        value = self.char["uvIndex"].read()
        value = ord(value)
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_co2(self):
        self.logger.debug("reading CO2 characteristic")
        value = self.char["co2"].read()
        value = struct.unpack("<h", value)
        value = value[0]
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_voc(self):
        self.logger.debug("reading VOC characteristic")
        value = self.char["voc"].read()
        value = struct.unpack("<h", value)
        value = value[0]
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_sound(self):
        self.logger.debug("reading sound characteristic")
        value = self.char["sound"].read()
        value = struct.unpack("<h", value)
        value = value[0] / 100
        return value

    @retry(stop_max_attempt_number=5, wait_fixed=500)
    def read_pressure(self):
        self.logger.debug("reading pressure characteristic")
        value = self.char["pressure"].read()
        value = struct.unpack("<L", value)
        value = value[0] / 1000
        return value
